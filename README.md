Instrucciones de la línea de comando
También puede subir archivos existentes desde su ordenador utilizando las instrucciones que se muestran a continuación.


Configuración global de Git
git config --global user.name "Jimmy Mayta"
git config --global user.email "jimmyymaytaj@gmail.com"

Crear un nuevo repositorio
git clone https://gitlab.com/JimmyMayta/sistema.git
cd sistema
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Push a una carpeta existente
cd existing_folder
git init
git remote add origin https://gitlab.com/JimmyMayta/sistema.git
git add .
git commit -m "Initial commit"
git push -u origin master

Push a un repositorio de Git existente
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/JimmyMayta/sistema.git
git push -u origin --all
git push -u origin --tags







